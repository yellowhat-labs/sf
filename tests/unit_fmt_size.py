#!/usr/bin/env python3
"""UnitTest fmt_size function"""

from importlib.machinery import SourceFileLoader
from importlib.util import spec_from_loader, module_from_spec
from unittest import main, TestCase

loader = SourceFileLoader(fullname="sf.py", path="sf.py")
spec = spec_from_loader(loader.name, loader)
sf = module_from_spec(spec)
loader.exec_module(sf)


class TestProcess(TestCase):
    """Test"""

    def test_0(self) -> None:
        """Size 0"""

        out = sf.fmt_size(0)
        self.assertEqual(out, "     0  B")

    def test_size_kb(self) -> None:
        """Size kiloBytes"""

        size_kb = 1024

        out = sf.fmt_size(size_kb)
        self.assertEqual(out, "     1 kB")

        out = sf.fmt_size(2 * size_kb)
        self.assertEqual(out, "     2 kB")

        out = sf.fmt_size(10 * size_kb)
        self.assertEqual(out, "    10 kB")

        out = sf.fmt_size(100 * size_kb)
        self.assertEqual(out, "   100 kB")

        out = sf.fmt_size(10 * 1000)
        self.assertEqual(out, "    10 kB")

    def test_size_mb(self) -> None:
        """Size MegaBytes"""

        size_mb = 1024**2

        out = sf.fmt_size(size_mb)
        self.assertEqual(out, "   1.0 MB")

        out = sf.fmt_size(2 * size_mb)
        self.assertEqual(out, "   2.0 MB")

        out = sf.fmt_size(10 * size_mb)
        self.assertEqual(out, "  10.0 MB")

        out = sf.fmt_size(100 * size_mb)
        self.assertEqual(out, " 100.0 MB")

        out = sf.fmt_size(999 * size_mb)
        self.assertEqual(out, " 999.0 MB")

        out = sf.fmt_size(999 * 1000**2)
        self.assertEqual(out, " 952.7 MB")

    def test_size_gb(self) -> None:
        """Size GigaBytes"""

        size_gb = 1024**3

        out = sf.fmt_size(size_gb)
        self.assertEqual(out, "  1.00 GB")

        out = sf.fmt_size(2 * size_gb)
        self.assertEqual(out, "  2.00 GB")

        out = sf.fmt_size(10 * size_gb)
        self.assertEqual(out, " 10.00 GB")

        out = sf.fmt_size(100 * size_gb)
        self.assertEqual(out, "100.00 GB")

        out = sf.fmt_size(999 * size_gb)
        self.assertEqual(out, "999.00 GB")

        out = sf.fmt_size(999 * 1000**3)
        self.assertEqual(out, "930.39 GB")

    def test_size_tb(self) -> None:
        """Size TeraBytes"""

        size_tb = 1024**4

        out = sf.fmt_size(size_tb)
        self.assertEqual(out, "  1.00 TB")

        out = sf.fmt_size(2 * size_tb)
        self.assertEqual(out, "  2.00 TB")

        out = sf.fmt_size(10 * size_tb)
        self.assertEqual(out, " 10.00 TB")

        out = sf.fmt_size(100 * size_tb)
        self.assertEqual(out, "100.00 TB")

        out = sf.fmt_size(999 * size_tb)
        self.assertEqual(out, "999.00 TB")

        out = sf.fmt_size(999 * 1000**4)
        self.assertEqual(out, "908.59 TB")

    def test_size_pb(self) -> None:
        """Size PetaBytes"""

        size_pb = 1024**5

        out = sf.fmt_size(size_pb)
        self.assertEqual(out, "  1.00 PB")

        out = sf.fmt_size(2 * size_pb)
        self.assertEqual(out, "  2.00 PB")

        out = sf.fmt_size(10 * size_pb)
        self.assertEqual(out, " 10.00 PB")

        out = sf.fmt_size(100 * size_pb)
        self.assertEqual(out, "100.00 PB")

        out = sf.fmt_size(999 * size_pb)
        self.assertEqual(out, "999.00 PB")

        out = sf.fmt_size(999 * 1000**5)
        self.assertEqual(out, "887.29 PB")


if __name__ == "__main__":
    main()
